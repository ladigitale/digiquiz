# Digiquiz

Digiquiz est une interface simple pour lire des contenus H5P en ligne avec H5P Standalone (https://github.com/tunapanda/h5p-standalone).

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte HKGrotesk (Sil Open Font Licence 1.1) et H5P Standalone - https://github.com/tunapanda/h5p-standalone - (MIT Licence)

Pour faciliter le déploiement, ce dépôt contient également toutes les librairies H5P - https://github.com/h5p - (MIT Licence)

### Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run dev
```

### Variable d'environnement (fichier .env.production à créer à la racine avant compilation)
```
AUTHORIZED_DOMAINS (* ou liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule)
VITE_DOMAIN (hôte de l'application, par exemple https://ladigitale.dev)
VITE_FOLDER (dossier de l'application, par exemple /digiquiz/)
VITE_LIEN_RESULTATS (lien pour le traitement des résultats, par exemple http://localhost:8081/inc/enregistrer_resultats.php)
```

### Compilation et minification des fichiers
```
npm run build
```

### Serveur PHP nécessaire pour l'API
```
php -S 127.0.0.1:8000 (pour le développement uniquement)
```

### Configuration .htaccess pour serveur Apache (production)
```
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.html
```

### Démo
https://ladigitale.dev/digiquiz/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/
