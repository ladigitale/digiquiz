import { createRouter, createWebHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Quiz from '../views/Quiz.vue'

const routes = [
	{
		path: '/',
		name: 'Accueil',
		component: Accueil
	},
	{
		path: '/q/:id',
		name: 'Quiz',
		component: Quiz
	}
]

let router
if (import.meta.env.VITE_FOLDER) {
	router = createRouter({
		history: createWebHistory(import.meta.env.VITE_FOLDER),
		routes
	})
} else {
	router = createRouter({
		history: createWebHistory(),
		routes
	})
}

export default router
