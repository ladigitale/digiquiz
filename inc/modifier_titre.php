<?php

session_start();

require 'headers.php';

if (!empty($_POST['id']) && !empty($_POST['nouveautitre'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digiquiz'][$id]['reponse'])) {
		$reponse = $_SESSION['digiquiz'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digiquiz_contenus WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		$resultat = $stmt->fetchAll();
		if ($resultat[0]['reponse'] === $reponse) {
			$nouveautitre = $_POST['nouveautitre'];
			$stmt = $db->prepare('UPDATE digiquiz_contenus SET titre = :nouveautitre WHERE url = :url');
			if ($stmt->execute(array('nouveautitre' => $nouveautitre, 'url' => $id))) {
				echo 'titre_modifie';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
