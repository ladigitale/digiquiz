<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digiquiz'][$id]['reponse'])) {
		$reponse = $_SESSION['digiquiz'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digiquiz_contenus WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($contenu = $stmt->fetchAll()) {
			$admin = false;
			if (count($contenu, COUNT_NORMAL) > 0 && $contenu[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$digidrive = 0;
			if (isset($_SESSION['digiquiz'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digiquiz'][$id]['digidrive'];
			} else if (intval($contenu[0]['digidrive']) === 1) {
				$digidrive = 1;
			}
			$titre = $contenu[0]['titre'];
			$donnees = $contenu[0]['donnees'];
			$date = date('Y-m-d H:i:s');
			$vues = 0;
			if ($contenu[0]['vues'] !== '') {
				$vues = intval($contenu[0]['vues']);
			}
			if ($admin === false) {
				$vues = $vues + 1;
			}
			if ($donnees === '' || $donnees === null) {
				if (file_exists('../fichiers/' . $id . '/h5p/h5p.json')) {
					$resultats = false;
					if (file_exists('../fichiers/' . $id . '/index.html')) {
						$html = file_get_contents('../fichiers/' . $id . '/index.html');
						if ($html !== false && $html !== null && strpos($html, 'resultatsActives = "true"') !== false) {
							$resultats = true;
						}
					}
					$h5p = '../fichiers/' . $id . '/h5p';
					$h5pJson = file_get_contents($h5p . '/h5p.json');
					$json = json_decode($h5pJson, true);
					if ($titre === '') {
						$titre = $json['title'];
					}
					$donnees = json_encode(array('typeH5P' => $json['mainLibrary'], 'resultatsActives' => $resultats));
					$stmt = $db->prepare('UPDATE digiquiz_contenus SET titre = :titre, donnees = :donnees, vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
					if ($stmt->execute(array('titre' => $titre, 'donnees' => $donnees, 'vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
						echo json_encode(array('titre' => $titre, 'donnees' => $donnees, 'vues' => $vues, 'admin' =>  $admin, 'digidrive' => $digidrive));
					} else {
						echo 'erreur';
					}
				} else {
					echo 'contenu_inexistant';
				}
			} else {
				$stmt = $db->prepare('UPDATE digiquiz_contenus SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
				if ($stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
					echo json_encode(array('titre' => $titre, 'donnees' => $donnees, 'vues' => $vues, 'admin' =>  $admin, 'digidrive' => $digidrive));
				} else {
					echo 'erreur';
				}
			}
		} else if (file_exists('../fichiers/' . $id . '/index.html')) {
			$html = file_get_contents('../fichiers/' . $id . '/index.html');
			$digidrive = 0;
			$resultats = false;
			if (strpos($html, 'resultatsActives = "true"') !== false) {
				$digidrive = 1;
				$resultats = true;
			}
			$h5p = '../fichiers/' . $id . '/h5p';
			$h5pJson = file_get_contents($h5p . '/h5p.json');
			$json = json_decode($h5pJson, true);
			$titre = $json['title'];
			$question = 'motPrefere';
			$reponse = password_hash($id, PASSWORD_DEFAULT);
			$donnees = json_encode(array('typeH5P' => $json['mainLibrary'], 'resultatsActives' => $resultats));
			$date = date('Y-m-d H:i:s');
			$vues = 0;
			$admin = false;
			$stmt = $db->prepare('INSERT INTO digiquiz_contenus (url, titre, question, reponse, donnees, date, vues, derniere_visite, digidrive) VALUES (:url, :titre, :question, :reponse, :donnees, :date, :vues, :derniere_visite, :digidrive)');
			if ($stmt->execute(array('url' => $id, 'titre' => $titre, 'question' => $question, 'reponse' => $reponse, 'donnees' => $donnees, 'date' => $date, 'vues' => $vues, 'derniere_visite' => $date, 'digidrive' => $digidrive))) {
				echo json_encode(array('titre' => $titre, 'donnees' => $donnees, 'vues' => $vues, 'admin' =>  $admin, 'digidrive' => $digidrive));
			} else {
				echo 'erreur';
			}
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
