﻿<?php

session_start();

require 'headers.php';

if (!empty($_FILES['fichier']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$id = uniqid('', false);
	if (is_dir('../fichiers/' . $id) === false) {
		$temp = '../fichiers/' . $id . '/temp';
		$h5p = '../fichiers/' . $id . '/h5p';
		mkdir('../fichiers/' . $id, 0775, true);
		mkdir($h5p, 0775, true);
		mkdir($temp, 0775, true);
		$fichier = $h5p . '/' . basename($_FILES['fichier']['name']);
		if (move_uploaded_file($_FILES['fichier']['tmp_name'], $fichier)) {
			$zip = new ZipArchive;
			if ($zip->open($fichier) === TRUE) {
				$zip->extractTo($temp);
				$zip->close();
				rename($temp . '/h5p.json', $h5p . '/h5p.json');
				rename($temp . '/content', $h5p . '/content');
				$h5pJson = file_get_contents($h5p . '/h5p.json');
				$json = json_decode($h5pJson, true);
				array_push($json['preloadedDependencies'], array('machineName' => 'H5P.MathDisplay', 'majorVersion' => 1, 'minorVersion' => 0));
				file_put_contents($h5p . '/h5p.json', json_encode($json));
				$titre = $json['title'];
				$question = $_POST['question'];
				$reponse = password_hash(strtolower($_POST['reponse']), PASSWORD_DEFAULT);
				$donnees = json_encode(array('typeH5P' => $json['mainLibrary']));
				$date = date('Y-m-d H:i:s');
				$vues = 0;
				$digidrive = 0;
				$stmt = $db->prepare('INSERT INTO digiquiz_contenus (url, titre, question, reponse, donnees, date, vues, derniere_visite, digidrive) VALUES (:url, :titre, :question, :reponse, :donnees, :date, :vues, :derniere_visite, :digidrive)');
				if ($stmt->execute(array('url' => $id, 'titre' => $titre, 'question' => $question, 'reponse' => $reponse, 'donnees' => $donnees, 'date' => $date, 'vues' => $vues, 'derniere_visite' => $date, 'digidrive' => $digidrive))) {
					unlink($fichier);
					supprimer($temp);
					$_SESSION['digiquiz'][$id]['reponse'] = $reponse;
					echo $id;
				} else {
					echo 'erreur';
				}
			} else {
				echo 'erreur_extraction';
			}
		} else {
			echo 'erreur_televersement';
		}
	} else {
		echo 'contenu_existe_deja';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

function supprimer ($path) {
	if (is_dir($path) === true) {
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file) {
			supprimer(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}
	return false;
}

?>
