﻿<?php

session_start();

require 'headers.php';

if (!empty($_FILES['fichier']) && !empty($_POST['id'])) {
	require 'db.php';
	$id = $_POST['id'];
	$temp = '../fichiers/' . $id . '/temp';
	$h5p = '../fichiers/' . $id . '/h5p';
	supprimer($h5p);
	mkdir($h5p, 0775, true);
	mkdir($temp, 0775, true);
	$fichier = $h5p . '/' . basename($_FILES['fichier']['name']);
	if (move_uploaded_file($_FILES['fichier']['tmp_name'], $fichier)) {
		$zip = new ZipArchive;
		if ($zip->open($fichier) === TRUE) {
			$zip->extractTo($temp);
			$zip->close();
			rename($temp . '/h5p.json', $h5p . '/h5p.json');
			rename($temp . '/content', $h5p . '/content');
			$h5pJson = file_get_contents($h5p . '/h5p.json');
			$json = json_decode($h5pJson, true);
			array_push($json['preloadedDependencies'], array('machineName' => 'H5P.MathDisplay', 'majorVersion' => 1, 'minorVersion' => 0));
			file_put_contents($h5p . '/h5p.json', json_encode($json));
			$stmt = $db->prepare('SELECT donnees FROM digiquiz_contenus WHERE url = :url');
			if ($stmt->execute(array('url' => $id))) {
				$resultat = $stmt->fetchAll();
				$donnees = json_decode($resultat[0]['donnees'], true);
				$donnees['typeH5P'] = $json['mainLibrary'];
				$donnees = json_encode($donnees);
				$stmt = $db->prepare('UPDATE digiquiz_contenus SET donnees = :donnees WHERE url = :url');
				if ($stmt->execute(array('donnees' => $donnees, 'url' => $id))) {
					unlink($fichier);
					supprimer($temp);
					echo 'contenu_importe';
				} else {
					echo 'erreur';
				}
			} else {
				echo 'erreur';
			}
		} else {
			echo 'erreur_extraction';
		}
	} else {
		echo 'erreur_televersement';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

function supprimer ($path) {
	if (is_dir($path) === true) {
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file) {
			supprimer(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}
	return false;
}

?>
