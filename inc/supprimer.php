<?php

session_start();

require 'headers.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['id'])) {
	require 'db.php';
	$id = $_POST['id'];
	$reponse = '';
	if (isset($_SESSION['digiquiz'][$id]['reponse'])) {
		$reponse = $_SESSION['digiquiz'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digiquiz_contenus WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else if ($resultat[0]['reponse'] === $reponse) {
			$stmt = $db->prepare('DELETE FROM digiquiz_contenus WHERE url = :url');
			if ($stmt->execute(array('url' => $id))) {
				if (file_exists('../fichiers/' . $id)) {
					supprimer('../fichiers/' . $id);
				}
				echo 'contenu_supprime';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

function supprimer ($path) {
	if (is_dir($path) === true) {
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file) {
			supprimer(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}
	return false;
}

?>
